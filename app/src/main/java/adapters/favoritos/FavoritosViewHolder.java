package adapters.favoritos;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import app.creepypastabrasil.com.br.creepypastabrasil.R;

/**
 * Created by alan on 23/01/18.
 */

public class FavoritosViewHolder extends RecyclerView.ViewHolder {

    final TextView titulo;

    public FavoritosViewHolder(View itemView) {
        super(itemView);
        titulo = itemView.findViewById(R.id.tvFavTitulo);
    }
}
