package repository;

import android.os.AsyncTask;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by aquadros on 24/01/2018.
 */

public class GooglePlusAPI extends AsyncTask<URL, Void, String> {

    @Override
    protected String doInBackground(URL... urls) {
        if(urls.length > 0){
            try {
                StringBuilder result = new StringBuilder();
                HttpURLConnection connection = (HttpURLConnection) urls[0].openConnection();
                connection.setRequestMethod("GET");
                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line = "";

                while ((line = reader.readLine()) != null){
                    result.append(line);
                }
                reader.close();
                line = result.toString();
                JSONObject json = new JSONObject(line);
                JSONObject imgJson = json.getJSONObject("image");

                String img = imgJson.getString("url");

                return img;

            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }

        }
        return null;
    }
}
