package util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import org.jsoup.Jsoup;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.Map;

/**
 * Created by alan on 14/01/18.
 */

public class Util {

    public static String adicionarQuebraDeLinha(String texto){
//        Document body = Jsoup.parse(texto);
//        body.outputSettings(new Document.OutputSettings().prettyPrint(false));
//        body.select("br").after("\\n");
//        body.select("p").before("\\n\\n");
//        String str = body.html().replaceAll("\\\\n", "\n");
//        return Jsoup.clean(str, "", Whitelist.none(), new Document.OutputSettings().prettyPrint(false));
        String body =
                Jsoup.parse(texto
                        .replaceAll("(?i)<br[^>]*>", "br2n")
                        .replaceAll("<li>", "br2n")
                        .replaceAll("</li>", "br2n"))
                        .text();

        body = body.replaceAll("br2n ", "br2n");
        body = body.replaceAll("br2nbr2n", "br2n");

        return body.replaceAll("br2n", "\n\n");
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }

    public static long sizeSharedPreferences(Map map) {
        try{
            System.out.println("Index Size: " + map.size());
            ByteArrayOutputStream baos=new ByteArrayOutputStream();
            ObjectOutputStream oos=new ObjectOutputStream(baos);
            oos.writeObject(map);
            oos.close();
            return baos.size();
        }catch(IOException e){
            e.printStackTrace();
        }
        return 0;
    }

    public static String readableFileSize(long size) {
        if(size <= 66) return "0 Bytes";
        final String[] units = new String[] { "Bytes", "kB", "MB", "GB", "TB" };
        int digitGroups = (int) (Math.log10(size)/Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size/Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    public static Drawable drawableFromUrl(String url) throws IOException {
        Bitmap x;

        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
        connection.connect();
        InputStream input = connection.getInputStream();

        x = BitmapFactory.decodeStream(input);
        return new BitmapDrawable(x);
    }
}
