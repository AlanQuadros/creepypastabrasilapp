package adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import app.creepypastabrasil.com.br.creepypastabrasil.R;

/**
 * Created by alan on 14/01/18.
 */

public class CreepypastaViewHolder extends RecyclerView.ViewHolder {

    final TextView data;
    final TextView titulo;
    final TextView intro;
    final CardView cardViewCreepypasta;

    public CreepypastaViewHolder(View itemView, Context context) {
        super(itemView);
        cardViewCreepypasta = itemView.findViewById(R.id.cardViewCreepypasta);
        data = itemView.findViewById(R.id.tvItemData);
        titulo = itemView.findViewById(R.id.tvItemTitulo);
        intro = itemView.findViewById(R.id.tvItemIntro);

        Typeface typefaceTitulo = Typeface.createFromAsset(context.getAssets(), "fonts/opensans_bold.ttf");
        titulo.setTypeface(typefaceTitulo);

        Typeface typefaceIntro = Typeface.createFromAsset(context.getAssets(), "fonts/opensans_regular.ttf");
        intro.setTypeface(typefaceIntro);

        Typeface typefaceData = Typeface.createFromAsset(context.getAssets(), "fonts/opensans_light.ttf");
        data.setTypeface(typefaceData);
    }
}
