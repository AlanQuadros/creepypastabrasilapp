package adapters.favoritos;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import activities.CreepypastaDetailActivity;
import activities.fragments.CreepypastaDetailFragment;
import app.creepypastabrasil.com.br.creepypastabrasil.R;
import models.Creepypasta;

/**
 * Created by alan on 23/01/18.
 */

public class FavoritosAdapter extends RecyclerView.Adapter {

    private List<Creepypasta> creepypastas;
    private Context context;

    public FavoritosAdapter(List<Creepypasta> creepypastas, Context context) {
        this.creepypastas = creepypastas;
        this.context = context;
    }

    private final View.OnClickListener mOnClickListener = new View.OnClickListener(){
        @Override
        public void onClick(View view) {
            Creepypasta item = (Creepypasta) view.getTag();

            Intent intent = new Intent(context, CreepypastaDetailActivity.class);
            intent.putExtra(CreepypastaDetailFragment.LINK, item.getLink());

            context.startActivity(intent);
        }
    };

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_favorito, parent, false);

        FavoritosViewHolder holder = new FavoritosViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        FavoritosViewHolder viewHolder = (FavoritosViewHolder) holder;
        Creepypasta creepypasta = creepypastas.get(position);

        viewHolder.titulo.setText(creepypasta.getTitulo());

        viewHolder.itemView.setTag(creepypastas.get(position));
        viewHolder.itemView.setOnClickListener(mOnClickListener);
    }

    @Override
    public int getItemCount() {
        return creepypastas.size();
    }
}
