package repository;

import android.provider.BaseColumns;

/**
 * Created by alan on 22/01/18.
 */

public final class CreepypastaContract {

    private CreepypastaContract(){}

    public static class CreepypastaEntry implements BaseColumns {
        public static final String NOME_TABELA = "creepypasta";
        public static final String COLUNA_TITULO = "titulo";
        public static final String COLUNA_LINK = "link";
    }
}
