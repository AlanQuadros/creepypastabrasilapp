package activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.TextAppearanceSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import activities.fragments.CreepypastaDetailFragment;
import adapters.CreepypastaAdapter;
import adapters.EndlessRecyclerOnScrollListener;
import app.creepypastabrasil.com.br.creepypastabrasil.R;
import models.Creepypasta;
import util.Util;

public class CreepypastaListActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    public static final String PREFS_CREEPY = "PREFSCREEPY";

    private boolean mTwoPane;
    private boolean primeiraAbertura = true;

    private List<Creepypasta> creepypastas;
    private Document htmlDocument;
    private String htmlPageUrl = "http://www.creepypastabrasil.com.br/?m=1";
    private String urlProximaPagina;
    private Elements conteudo;
    private ProgressBar progressBarLista;
    private CreepypastaAdapter creepypastaAdapter;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layout;
    private ImageView ivRefresh;
    private MenuItem limparCache;
    private NavigationView navigationView;
    private CheckBox cbOcultarLidas;

    private CarregarCreepysAsync carregarCreepysAsync;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creepypasta_list);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        progressBarLista = findViewById(R.id.progressBarLista);
        recyclerView = findViewById(R.id.creepypasta_list);
        ivRefresh = findViewById(R.id.ivRefresh);

        assert recyclerView != null;

        sharedPreferences = getSharedPreferences(PREFS_CREEPY, Context.MODE_PRIVATE);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        progressBarLista.getIndeterminateDrawable().setColorFilter(getResources()
                .getColor(R.color.colorRed), PorterDuff.Mode.SRC_IN);

        ivRefresh.setVisibility(View.GONE);
        ivRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ivRefresh.setVisibility(View.GONE);
                progressBarLista.setVisibility(View.VISIBLE);
                primeiraAbertura = true;
                creepypastas = new ArrayList<>();
                CarregarCreepysAsync carregarCreepysAsync = new CarregarCreepysAsync();
                carregarCreepysAsync.execute(htmlPageUrl);
            }
        });

        if(Util.isOnline(this)){
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();

            navigationView = findViewById(R.id.nav_view);

            Menu menu = navigationView.getMenu();
            MenuItem categorias = menu.findItem(R.id.item_categorias);
            SpannableString s = new SpannableString(categorias.getTitle());
            s.setSpan(new TextAppearanceSpan(this, R.style.NavTextItemColor), 0, s.length(), 0);
            categorias.setTitle(s);

            MenuItem configuracoes = menu.findItem(R.id.item_configuracoes);
            s = new SpannableString(configuracoes.getTitle());
            s.setSpan(new TextAppearanceSpan(this, R.style.NavTextItemColor), 0, s.length(), 0);
            configuracoes.setTitle(s);

            limparCache = menu.findItem(R.id.nav_limpar_cache);

            Map<String,?> keys = sharedPreferences.getAll();

            long cacheSize = Util.sizeSharedPreferences(keys);
            cacheTitulo(cacheSize);

            //Inicio Switch Ocultar lidas
            MenuItem menuItem = menu.findItem(R.id.nav_ocultar_lidas);
            View actionView = MenuItemCompat.getActionView(menuItem);
            cbOcultarLidas = actionView.findViewById(R.id.cbOcultarLidas);
            cbOcultarLidas.setChecked(sharedPreferences.getBoolean("ocultar_lidas", false));

            cbOcultarLidas.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putBoolean("ocultar_lidas", cbOcultarLidas.isChecked());
                    editor.commit();
                    creepypastaAdapter.notifyDataSetChanged();
                }
            });
            //Fim Switch Ocultar lidas

            navigationView.setItemIconTintList(null);
            navigationView.setNavigationItemSelectedListener(this);

            creepypastas = new ArrayList<>();

            carregarCreepysAsync = new CarregarCreepysAsync();
            carregarCreepysAsync.execute(htmlPageUrl);

            if (findViewById(R.id.creepypasta_detail_container) != null) {
                mTwoPane = true;
            }
        } else {
            progressBarLista.setVisibility(View.GONE);
            Snackbar snackbar = Snackbar.make(
                    findViewById(R.id.listaCreepys),
                    "Sem conexão com a internet.",
                    Snackbar.LENGTH_LONG);
            snackbar.show();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ivRefresh.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    private void cacheTitulo(long cache){
        SpannableString s = new SpannableString("Limpar memória cache:   "+Util.readableFileSize(cache));
        s.setSpan(new ForegroundColorSpan(Color.rgb(204,0,0)), 23, s.length(), 0);
        s.setSpan(new RelativeSizeSpan(0.7f), 23,s.length(), 0);

        limparCache.setTitle(s);
    }

    private long tamanhoCache() {
        long size=0;

        File[] files = getCacheDir().listFiles();
        for (File f:files) {
            size = size+f.length();
        }

        return size;
    }

    private static void cleanDir(File dir, long bytes) {

        long bytesDeleted = 0;
        File[] files = dir.listFiles();

        for (File file : files) {
            bytesDeleted += file.length();
            file.delete();

            if (bytesDeleted >= bytes) {
                break;
            }
        }
    }

    private void setupRecyclerView(@NonNull final RecyclerView recyclerView) {

        layout = new LinearLayoutManager(getBaseContext());
        recyclerView.setLayoutManager(layout);
        creepypastaAdapter = new CreepypastaAdapter(creepypastas, this);
        recyclerView.setAdapter(creepypastaAdapter);

        recyclerView.setOnScrollListener(new EndlessRecyclerOnScrollListener((LinearLayoutManager) layout) {
            @Override
            public void onLoadMore(int current_page) {
                progressBarLista.setVisibility(View.VISIBLE);

                CarregarCreepysAsync carregarCreepysAsync = new CarregarCreepysAsync();
                carregarCreepysAsync.execute(urlProximaPagina);
            }
        });
    }

    private void carregarCreepys(String url){
        try {
            htmlDocument = Jsoup.connect(url).timeout(30 * 1000).get();

            conteudo = htmlDocument.select("div.blog-posts");

            Elements creepys = conteudo.select("div.mobile-date-outer");

            for (Element creepy : creepys) {
                Creepypasta creepypasta = new Creepypasta();

                if(creepy.selectFirst("div.date-header") != null){
                    creepypasta.setData(creepy.selectFirst("div.date-header").text());
                }

                creepypasta.setTitulo(creepy.selectFirst("h3").text());
                creepypasta.setIntro(creepy.selectFirst("div.post-body").text());
                creepypasta.setLink(creepy.getElementsByTag("a").first().attr("href"));

                if(sharedPreferences.getBoolean(creepypasta.getLink(), false)){
                    creepypasta.setVisitada(true);
                } else {
                    creepypasta.setVisitada(false);
                }

                creepypastas.add(creepypasta);
            }

            if(htmlDocument.selectFirst("a.blog-pager-older-link") != null){
                urlProximaPagina = htmlDocument.selectFirst("a.blog-pager-older-link").absUrl("href");
            }
        } catch (IOException e) {
            e.printStackTrace();
            Snackbar snackbar = Snackbar.make(
                    findViewById(R.id.listaCreepys),
                    "Algo errado aconteceu :(",
                    Snackbar.LENGTH_LONG);
            snackbar.show();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ivRefresh.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.nav_facebook){
            Intent intent;
            try {
                getBaseContext().getPackageManager().getPackageInfo("com.facebook.katana", 0);
                intent =  new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/292958527405986"));
            } catch (Exception e) {
                intent = new Intent(getBaseContext(), WebViewActivity.class);
                intent.putExtra("titulo", "Entre em contato");
                intent.putExtra("url", "https://www.facebook.com/creepypastabrasil/");
            }
            startActivity(intent);
        }

        progressBarLista.setVisibility(View.VISIBLE);
        final CarregarCreepysAsync carregarCreepysAsync = new CarregarCreepysAsync();
        primeiraAbertura = true;
        creepypastas = new ArrayList<>();

        if(id == R.id.nav_limpar_cache){
            AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.CustomAlertDialog);
            final View view = getLayoutInflater().inflate(R.layout.dialog_limpar_cache, null);
            builder.setView(view);

            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    cleanDir(getCacheDir(), tamanhoCache());

                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.clear();
                    editor.commit();

                    Map<String,?> keys = sharedPreferences.getAll();
                    cacheTitulo(Util.sizeSharedPreferences(keys));

                    carregarCreepysAsync.execute(htmlPageUrl);
                }
            });

            builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    progressBarLista.setVisibility(View.GONE);
                }
            });

            final AlertDialog dialog = builder.create();

            dialog.show();
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.rgb(204,0,0));
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.rgb(204,0,0));

        } else if (id == R.id.nav_mindfuck) {
            carregarCreepysAsync.execute("http://www.creepypastabrasil.com.br/search/label/Mindfuck?m=1");
            setTitle("Mindfuck");
        } else if (id == R.id.nav_creepyfas) {
            carregarCreepysAsync.execute("http://www.creepypastabrasil.com.br/search/label/Creepypasta%20dos%20F%C3%A3s?m=1");
            setTitle("Creepypasta dos Fãs");
        } else if (id == R.id.nav_videogames) {
            carregarCreepysAsync.execute("http://www.creepypastabrasil.com.br/search/label/Video%20Games?m=1");
            setTitle("Video Games");
        } else if (id == R.id.nav_creeper_semana) {
            carregarCreepysAsync.execute("http://www.creepypastabrasil.com.br/search/label/Creeper%20da%20Semana?m=1");
            setTitle("Creeper da Semana");
        } else if (id == R.id.nav_episodios_perdidos) {
            carregarCreepysAsync.execute("http://www.creepypastabrasil.com.br/search/label/Epis%C3%B3dios%20Perdidos?m=1");
            setTitle("Episódios Perdidos");
        } else if (id == R.id.nav_comunicados_atualizacoes) {
            carregarCreepysAsync.execute("http://www.creepypastabrasil.com.br/search/label/Comunicados%20e%20Atualiza%C3%A7%C3%B5es?m=1");
            setTitle("Comunicados e Atualizações");
//        } else if (id == R.id.nav_18) {
//            carregarPagina("http://www.creepypastabrasil.com.br/search/label/%2B18?m=1");
//            setTitle("+18");
        } else if (id == R.id.nav_videos) {
            carregarCreepysAsync.execute("http://www.creepypastabrasil.com.br/search/label/V%C3%ADdeos?m=1");
            setTitle("Vídeos");
        } else if (id == R.id.nav_trollpasta) {
            carregarCreepysAsync.execute("http://www.creepypastabrasil.com.br/search/label/Trollpasta?m=1");
            setTitle("Trollpasta");
        } else if (id == R.id.nav_pagina_inicial) {
            carregarCreepysAsync.execute(htmlPageUrl);
            setTitle("Creepypasta Brasil");
        } else if (id == R.id.nav_quem_somos) {
            Intent intent = new Intent(getBaseContext(), CreepypastaDetailActivity.class);
            intent.putExtra(CreepypastaDetailFragment.LINK, "http://www.creepypastabrasil.com.br/p/fala-galera-tudo-tranquilo-bem.html?m=1");
            startActivity(intent);
        } else if (id == R.id.nav_contato) {
            Intent intent = new Intent(getBaseContext(), CreepypastaDetailActivity.class);
            intent.putExtra(CreepypastaDetailFragment.LINK, "http://www.creepypastabrasil.com.br/p/contatos.html?m=1");
            startActivity(intent);
        } else if (id == R.id.nav_perguntas_respostas) {
            Intent intent = new Intent(getBaseContext(), CreepypastaDetailActivity.class);
            intent.putExtra(CreepypastaDetailFragment.LINK, "http://www.creepypastabrasil.com.br/p/o-que-e-creepypasta.html?m=1");
            startActivity(intent);
        } else if(id == R.id.nav_favoritos) {
            Intent intent = new Intent(getBaseContext(), FavoritosActivity.class);
            startActivity(intent);
        } else if(id == R.id.nav_ocultar_lidas){
            progressBarLista.setVisibility(View.GONE);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private class CarregarCreepysAsync extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            carregarCreepys(params[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(primeiraAbertura){
                primeiraAbertura = false;
                setupRecyclerView(recyclerView);
            } else {
                creepypastaAdapter.notifyDataSetChanged();
            }
            progressBarLista.setVisibility(View.GONE);
        }
    }
}
