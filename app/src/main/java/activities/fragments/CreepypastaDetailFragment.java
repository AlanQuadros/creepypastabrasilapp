package activities.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareButton;
import com.github.clans.fab.FloatingActionButton;
import com.squareup.picasso.Picasso;

import net.opacapp.multilinecollapsingtoolbar.CollapsingToolbarLayout;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import activities.CreepypastaDetailActivity;
import activities.WebViewActivity;
import app.creepypastabrasil.com.br.creepypastabrasil.R;
import de.hdodenhof.circleimageview.CircleImageView;
import models.Creepypasta;
import repository.CreepypastaDAO;
import repository.GooglePlusAPI;
import util.CustomTypefaceSpan;
import util.Util;

import static util.Util.adicionarQuebraDeLinha;

public class CreepypastaDetailFragment extends Fragment {

    public static final String PREFS_CREEPY = "PREFSCREEPY";
    public static final String TamanhoTexto = "chaveTamanhoTexto";
    public static final String CorFundo = "corDeFundo";

    public static final String LINK = "item_id";
    private Document htmlDocument;
    private String htmlPageUrl;
    private Elements titulo;
    private Elements texto;
    private String paragrafosDoTexto;
    private String urlImagem;
    private SpannableString textoComLinks;
    private String nomeAutor;
    private String imgAutor;

    private ProgressBar progressBar;
    private NestedScrollView scrollCreepypastaDetail;
    private FloatingActionButton fonteMenor;
    private FloatingActionButton fonteMaior;
    private FloatingActionButton inverterCores;
    private TextView tvTexto;
    private ImageView ivImagemInicio;
    private ImageView ivImagemFinal;
    private ImageView ivFavorito;
    private ShareButton shareButton;

    private SharedPreferences sharedPreferences;

    public CreepypastaDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);

        sharedPreferences = getContext().getSharedPreferences(PREFS_CREEPY, Context.MODE_PRIVATE);
        final CreepypastaDAO creepypastaDAO = new CreepypastaDAO(getContext());

        htmlPageUrl = getArguments().getString(LINK);

        progressBar = this.getActivity().findViewById(R.id.progressBarNovo);
        scrollCreepypastaDetail = this.getActivity().findViewById(R.id.creepypasta_detail_container);
        fonteMaior = this.getActivity().findViewById(R.id.menu_item_mais);
        fonteMenor = this.getActivity().findViewById(R.id.menu_item_menos);
        inverterCores = this.getActivity().findViewById(R.id.menu_item_inverter);
        ivFavorito = this.getActivity().findViewById(R.id.ivFavorito);

        if(progressBar != null){
            progressBar.setVisibility(View.VISIBLE);
            progressBar.getIndeterminateDrawable().setColorFilter(getResources()
                    .getColor(R.color.colorRed), PorterDuff.Mode.SRC_IN);
        }

        if(fonteMenor != null){
            fonteMaior.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    tvTexto.setTextSize(TypedValue.COMPLEX_UNIT_PX, (float) (tvTexto.getTextSize()+4.0));
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putFloat(TamanhoTexto, tvTexto.getTextSize());
                    editor.commit();
                }
            });

            fonteMenor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    tvTexto.setTextSize(TypedValue.COMPLEX_UNIT_PX, tvTexto.getTextSize()-4);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putFloat(TamanhoTexto, tvTexto.getTextSize());
                    editor.commit();
                }
            });

            inverterCores.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(tvTexto.getCurrentTextColor() == Color.WHITE){
                        getActivity().findViewById(R.id.creepypasta_detail).setBackgroundColor(Color.WHITE);
                        tvTexto.setTextColor(Color.BLACK);
//                    tvTitulo.setTextColor(Color.BLACK);

                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean(CorFundo, true);
                        editor.commit();
                    } else {
                        getActivity().findViewById(R.id.creepypasta_detail).setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                        tvTexto.setTextColor(Color.WHITE);
//                    tvTitulo.setTextColor(Color.WHITE);

                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean(CorFundo, false);
                        editor.commit();
                    }
                }
            });

            ivFavorito.setVisibility(View.GONE);
            if(creepypastaDAO.verificarCreepyEstaFavoritada(htmlPageUrl)){
                ivFavorito.setBackgroundResource(R.drawable.fav);
            }

            ivFavorito.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Creepypasta creepypasta = new Creepypasta();
                    creepypasta.setTitulo(titulo.text());
                    creepypasta.setLink(htmlPageUrl);

                    long id = creepypastaDAO.favoritarCreepy(creepypasta);

                    Snackbar snackbar;
                    if(id > 0){
                        ivFavorito.setBackgroundResource(R.drawable.fav);
                        snackbar = Snackbar.make(
                                getView(),
                                "Creepypasta adicionada aos favoritos :)",
                                Snackbar.LENGTH_LONG);
                    } else {
                        ivFavorito.setBackgroundResource(R.drawable.not_fav);
                        snackbar = Snackbar.make(
                                getView(),
                                "Creepypasta removida dos favoritos :(",
                                Snackbar.LENGTH_LONG);
                    }
                    View sbView = snackbar.getView();
                    sbView.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorRed));

                    snackbar.show();
                }
            });
        }
    }

    private void mudaCoresTexto(String paragrafos, Elements textoCompleto){
        Elements textColorSpan = textoCompleto.select("span[style*=color]");
        Elements textColorB = textoCompleto.select("b[style*=color]");

        preencherCor(textColorSpan, paragrafos, false);

        if(textColorB.size() > 0){
            preencherCor(textColorB, paragrafos, true);
        }
    }

    private void preencherCor(Elements textColor, String paragrafos, boolean tagB){
        for (Element color : textColor) {
            String textoCor = color.text();

            if(textoCor == "") return;

            int inicio = paragrafos.indexOf(textoCor);

            if(inicio < 0) return;

            ForegroundColorSpan[] span = textoComLinks.getSpans(
                    inicio,
                    inicio + textoCor.length(),
                    ForegroundColorSpan.class);

            inicio = recursaoSpan(span, inicio, paragrafos, textoCor);

            if(tagB){
                Typeface negritoFont =
                        Typeface.createFromAsset(
                                getContext().getAssets(),
                                "fonts/opensans_bold.ttf");
                textoComLinks.setSpan(
                        new CustomTypefaceSpan("", negritoFont),
                        inicio,
                        inicio + textoCor.length(),
                        0);
            }

            textoComLinks.setSpan(
                    new ForegroundColorSpan(Color.rgb(204, 0, 0)),
                    inicio,
                    inicio + textoCor.length(),
                    0);
        }
    }

    private void geraTexto(){
        Elements negritos = texto.select("b");
        Elements italicos = texto.select("i");
        Elements italicosNegritos = negritos.select("i");

        if(italicosNegritos.size() > 0){
            geraTextoItalicoNegrito();
        } else if(negritos.size() > 0) {
            geraTextoNegrito(negritos);
        } else if(italicos.size() > 0){
            geraTextoItalico(italicos);
        }
    }

    private void geraTextoNegrito(Elements negritos){
        for (Element negrito : negritos) {
            String textoNegrito = negrito.text();

            if(textoNegrito == "") return;

            int inicio = paragrafosDoTexto.indexOf(textoNegrito);

            CustomTypefaceSpan[] span = textoComLinks.getSpans(
                    inicio,
                    inicio + textoNegrito.length(),
                    CustomTypefaceSpan.class);

            inicio = recursaoSpan(span, inicio, paragrafosDoTexto, textoNegrito);

            if(inicio < 0) return;

            Typeface negritoFont =
                    Typeface.createFromAsset(
                            getContext().getAssets(),
                            "fonts/opensans_bold.ttf");

            textoComLinks.setSpan(
                    new CustomTypefaceSpan("", negritoFont),
                    inicio,
                    inicio + textoNegrito.length(),
                    0);
        }
    }

    private void geraTextoItalico(Elements italicos){
        for (Element italico : italicos) {
            String textoItalico = italico.text();

            if(textoItalico == "") return;

            int inicio = paragrafosDoTexto.indexOf(textoItalico);

            CustomTypefaceSpan[] span = textoComLinks.getSpans(
                    inicio,
                    inicio + textoItalico.length(),
                    CustomTypefaceSpan.class);

            inicio = recursaoSpan(span, inicio, paragrafosDoTexto, textoItalico);

            Typeface italicoFont =
                    Typeface.createFromAsset(
                            getContext().getAssets(),
                            "fonts/opensans_italic.ttf");

            textoComLinks.setSpan(
                    new CustomTypefaceSpan("", italicoFont),
                    inicio,
                    inicio + textoItalico.length(),
                    0);
        }
    }

    private void geraTextoItalicoNegrito(){

        Elements negritos = texto.select("b");
        Elements italicosNegritos = negritos.select("i");

        for (Element italicosNegrito : italicosNegritos) {
            String textoItalicoNegrito = italicosNegrito.text();

            if(textoItalicoNegrito == "") return;

            int inicio = paragrafosDoTexto.indexOf(textoItalicoNegrito);

            CustomTypefaceSpan[] span = textoComLinks.getSpans(
                    inicio,
                    inicio + textoItalicoNegrito.length(),
                    CustomTypefaceSpan.class);

//            inicio = recursaoSpan(span, inicio, paragrafos, textoItalicoNegrito);

            Typeface italicoNegiroFont =
                    Typeface.createFromAsset(
                            getContext().getAssets(),
                            "fonts/opensans_bolditalic.ttf");

            textoComLinks.setSpan(
                    new CustomTypefaceSpan("", italicoNegiroFont),
                    inicio,
                    inicio + textoItalicoNegrito.length(),
                    0);
        }
    }

    private ClickableSpan cliqueLink(final String linkUrl, final String textoLink){
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                if(linkUrl.contains("www.creepypastabrasil.com.br")){
                    Intent intent = new Intent(getContext(), CreepypastaDetailActivity.class);
                    intent.putExtra(CreepypastaDetailFragment.LINK, linkUrl);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getContext(), WebViewActivity.class);
                    intent.putExtra("titulo", textoLink);
                    intent.putExtra("url", linkUrl);
                    startActivity(intent);
                }
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };

        return clickableSpan;
    }

    private void gerarLinks(String paragrafos, Elements textoCompleto){

        Elements links = textoCompleto.select("a");

        for (final Element link : links) {

            final String linkUrl = link.absUrl("href");

            final String textoLink = link.text();

            int inicio = paragrafos.indexOf(textoLink);

            ClickableSpan[] span = textoComLinks.getSpans(
                    inicio,
                    inicio + textoLink.length(),
                    ClickableSpan.class);

            inicio = recursaoSpan(span, inicio, paragrafos, textoLink);

            textoComLinks.setSpan(
                    cliqueLink(linkUrl, textoLink),
                    inicio,
                    inicio + textoLink.length(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
    }

    //Verifica se o span já existe, caso sim vai para o próximo índice com a mesma ocorrência
    private int recursaoSpan(Object[] span, int inicio, String paragrafos, String textoLink){
        if(span.length > 0){
            inicio = paragrafos.indexOf(textoLink, inicio+1);
            span = textoComLinks.getSpans(inicio, inicio + textoLink.length(), Object.class);
            recursaoSpan(span, inicio, paragrafos, textoLink);
        }
        return inicio;
    }

    private void carregarElementos(){
        try {
            htmlDocument = Jsoup.connect(htmlPageUrl).get();
            titulo = htmlDocument.getElementsByTag("h3");
            texto = htmlDocument.select("div.entry-content");

            //Inicio Buscando dados do autor
            Element autor = htmlDocument.select("a[rel='author']").first();

            if(autor != null) {
                nomeAutor = autor.text();
                //Se for Blogger
                if(autor.absUrl("href").contains("www.blogger.com")) {
                    Document paginaAutor = Jsoup.connect(autor.absUrl("href")).get();
                    Element fotoAutor = paginaAutor.getElementsByClass("photo").first();
                    imgAutor = fotoAutor.absUrl("src");

                //Se for Google+
                } else if(autor.absUrl("href").contains("plus.google.com")) {

                    String id = autor.absUrl("href").substring(24);

                    final URL plusUrl =
                            new URL("https://www.googleapis.com/plus/v1/people/"+
                                    id+"?fields=image&key="+
                                    getResources().getString(R.string.google_api_key));
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                imgAutor = new GooglePlusAPI().execute(plusUrl).get();
                            } catch (InterruptedException | ExecutionException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }

            //Fim Buscando dados do autor

            Element img = texto.select("img").first();

            if (img != null) {
                urlImagem = img.absUrl("src");
            }

            paragrafosDoTexto = adicionarQuebraDeLinha(texto.html());

            textoComLinks = new SpannableString(paragrafosDoTexto);

            if(getContext() != null){
                gerarLinks(paragrafosDoTexto, texto);
                geraTexto();
                mudaCoresTexto(paragrafosDoTexto, texto);
            }

        } catch (IOException e) {
            e.printStackTrace();
            Snackbar.make(
                    getView(),
                    "Algo errado aconteceu :(",
                    Snackbar.LENGTH_LONG
            ).show();
        }

        if (getArguments().containsKey(LINK)) {
            if(getContext() != null){
                Activity activity = this.getActivity();
                CollapsingToolbarLayout appBarLayout = activity.findViewById(R.id.toolbar_layout);
                if (appBarLayout != null) {
                    Typeface typeface =
                            Typeface.createFromAsset(getContext().getAssets(), "fonts/opensans_bold.ttf");
                    appBarLayout.setExpandedTitleTypeface(typeface);
                    appBarLayout.setTitle(titulo.text());
                }
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.creepypasta_detail, container, false);

        shareButton = rootView.findViewById(R.id.fb_share_button);

        if(Util.isOnline(getContext())){
            JsoupAsyncTask jsoupAsyncTask = new JsoupAsyncTask();
            jsoupAsyncTask.execute(rootView);
        } else {
            progressBar.setVisibility(View.GONE);
            Snackbar.make(
                    getView(),
                    "Sem conexão com a internet.",
                    Snackbar.LENGTH_LONG
            ).show();
        }

        return rootView;
    }

    private class JsoupAsyncTask extends AsyncTask<View, Void, View> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected View doInBackground(View... views) {
            carregarElementos();
            return views[0];
        }

        @Override
        protected void onPostExecute(View view) {
            if(getContext() != null){
                if (htmlPageUrl != null) {
                    tvTexto = view.findViewById(R.id.tvTextoFragment);
                    tvTexto.setText(textoComLinks);
                    tvTexto.setMovementMethod(LinkMovementMethod.getInstance());
                    tvTexto.setHighlightColor(Color.TRANSPARENT);

                    float textoTamanho = sharedPreferences.getFloat(TamanhoTexto, 26.0f);
                    tvTexto.setTextSize(TypedValue.COMPLEX_UNIT_PX, textoTamanho);
                    boolean corFundo = sharedPreferences.getBoolean(CorFundo, false);
                    if(corFundo){
                        getActivity().findViewById(R.id.creepypasta_detail).setBackgroundColor(Color.WHITE);
                        tvTexto.setTextColor(Color.BLACK);
//                  tvTitulo.setTextColor(Color.BLACK);
                    }
                }

                if(imgAutor != null){
                    TextView textEnviadoPor = view.findViewById(R.id.textEnviadoPor);
                    textEnviadoPor.setVisibility(View.VISIBLE);
                    TextView tvNomeAutor = view.findViewById(R.id.tvNomeAutor);
                    tvNomeAutor.setText(nomeAutor);
                    tvNomeAutor.setVisibility(View.VISIBLE);

                    CircleImageView ivFotoAutor = view.findViewById(R.id.ivFotoAutor);
                    Picasso.with(getContext())
                            .load(imgAutor)
                            .noFade()
                            .into(ivFotoAutor);
                }

                if(urlImagem != null){
                    if(texto.html().indexOf("<img") < 500){
                        ivImagemInicio = getActivity().findViewById(R.id.ivImagemI);
                        Glide.with(getContext())
                                .load(urlImagem)
                                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                .into(ivImagemInicio);
                    } else {
                        ivImagemFinal = getActivity().findViewById(R.id.ivImagemF);
                        Glide.with(getContext())
                                .load(urlImagem)
                                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                .into(ivImagemFinal);
                    }
                }

                if(progressBar != null){
                    progressBar.setVisibility(View.GONE);
                    ivFavorito.setVisibility(View.VISIBLE);
                    shareButton.setVisibility(View.VISIBLE);

                    ShareLinkContent content;
                    if(urlImagem != null){
                        content = new ShareLinkContent.Builder()
                                .setContentTitle(titulo.text())
                                .setImageUrl(Uri.parse(urlImagem))
                                .setContentUrl(Uri.parse(htmlPageUrl))
                                .build();
                    } else {
                        content = new ShareLinkContent.Builder()
                                .setContentTitle(titulo.text())
                                .setContentUrl(Uri.parse(htmlPageUrl))
                                .build();
                    }
                    shareButton.setShareContent(content);
                }

                Handler handler = new Handler();

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        int scrollPos = sharedPreferences.getInt(htmlPageUrl+"scroll", 0);
                        if(scrollCreepypastaDetail != null){
                            scrollCreepypastaDetail.scrollTo(0, scrollPos);
                        }
                    }
                }, 0);

                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean(htmlPageUrl, true);
                editor.commit();
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        salvarPosicao();
    }

    private void salvarPosicao(){
        if(scrollCreepypastaDetail != null){
            SharedPreferences.Editor editor = sharedPreferences.edit();
            int scrollPos = scrollCreepypastaDetail.getScrollY();
            editor.putInt(htmlPageUrl+"scroll", scrollPos);
            editor.commit();
        }
    }
}

