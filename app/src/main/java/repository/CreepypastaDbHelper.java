package repository;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by alan on 22/01/18.
 */

public class CreepypastaDbHelper extends SQLiteOpenHelper {

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_CREEPYS =
            "CREATE TABLE " + CreepypastaContract.CreepypastaEntry.NOME_TABELA + " (" +
                    CreepypastaContract.CreepypastaEntry._ID + " INTEGER PRIMARY KEY," +
                    CreepypastaContract.CreepypastaEntry.COLUNA_TITULO + TEXT_TYPE + COMMA_SEP +
                    CreepypastaContract.CreepypastaEntry.COLUNA_LINK + TEXT_TYPE + " )";

    private static final String SQL_DELETE_CREEPYS =
            "DROP TABLE IF EXISTS " + CreepypastaContract.CreepypastaEntry.NOME_TABELA;

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "CreepypastaBrasil.db";


    public CreepypastaDbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_CREEPYS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SQL_DELETE_CREEPYS);
        onCreate(sqLiteDatabase);
    }
}
