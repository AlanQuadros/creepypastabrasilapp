package adapters;

/**
 * Created by aquadros on 19/01/2018.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}
