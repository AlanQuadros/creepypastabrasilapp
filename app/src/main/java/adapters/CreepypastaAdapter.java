package adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import activities.CreepypastaDetailActivity;
import activities.fragments.CreepypastaDetailFragment;
import activities.CreepypastaListActivity;
import app.creepypastabrasil.com.br.creepypastabrasil.R;
import models.Creepypasta;

/**
 * Created by alan on 14/01/18.
 */

public class CreepypastaAdapter extends RecyclerView.Adapter {

    public static final String PREFS_CREEPY = "PREFSCREEPY";

    private CreepypastaListActivity mParentActivity;
    private List<Creepypasta> creepypastas;
    private Context context;
    private SharedPreferences sharedPreferences;
    private boolean mTwoPane;

    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Creepypasta item = (Creepypasta) view.getTag();

            if (mTwoPane) {
                Bundle arguments = new Bundle();
                arguments.putString(CreepypastaDetailFragment.LINK, item.getTitulo());
                CreepypastaDetailFragment fragment = new CreepypastaDetailFragment();
                fragment.setArguments(arguments);
                mParentActivity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.creepypasta_detail_container, fragment)
                        .commit();
            } else {
                Intent intent = new Intent(context, CreepypastaDetailActivity.class);
                intent.putExtra(CreepypastaDetailFragment.LINK, item.getLink());

                context.startActivity(intent);
            }
        }
    };

    public CreepypastaAdapter(List<Creepypasta> creepypastas, Context context) {
        this.creepypastas = creepypastas;
        this.context = context;
        this.sharedPreferences = context.getSharedPreferences(PREFS_CREEPY, Context.MODE_PRIVATE);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_creepypasta, parent, false);
        return new CreepypastaViewHolder(view, context);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        CreepypastaViewHolder viewHolder = (CreepypastaViewHolder) holder;
        Creepypasta creepypasta = creepypastas.get(position);

        if(creepypasta.isVisitada()){
            if(sharedPreferences.getBoolean("ocultar_lidas", false)){
                viewHolder.cardViewCreepypasta.setVisibility(View.GONE);
            } else {
                viewHolder.cardViewCreepypasta.setVisibility(View.VISIBLE);
                viewHolder.cardViewCreepypasta.setAlpha((float) 0.5);
            }
        } else {
            viewHolder.cardViewCreepypasta.setAlpha((float) 1.0);
        }
        viewHolder.data.setText(creepypasta.getData());
        viewHolder.titulo.setText(creepypasta.getTitulo());
        viewHolder.intro.setText(creepypasta.getIntro());

        viewHolder.itemView.setTag(creepypastas.get(position));
        viewHolder.itemView.setOnClickListener(mOnClickListener);
    }

    @Override
    public int getItemCount() {
        return creepypastas.size();
    }
}
