package repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import models.Creepypasta;

/**
 * Created by alan on 23/01/18.
 */

public class CreepypastaDAO {

    private CreepypastaDbHelper creepypastaDbHelper;
    private Context context;

    public CreepypastaDAO(Context context) {
        this.context = context;
        this.creepypastaDbHelper = new CreepypastaDbHelper(context);
    }

    public long favoritarCreepy(Creepypasta creepypasta){

        SQLiteDatabase db = creepypastaDbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(CreepypastaContract.CreepypastaEntry.COLUNA_TITULO, creepypasta.getTitulo());
        values.put(CreepypastaContract.CreepypastaEntry.COLUNA_LINK, creepypasta.getLink());

        if(verificarCreepyEstaFavoritada(creepypasta.getLink())){
            String selection = CreepypastaContract.CreepypastaEntry.COLUNA_LINK + " LIKE ?";
            String[] selectionArgs = { creepypasta.getLink() };

            db.delete(CreepypastaContract.CreepypastaEntry.NOME_TABELA, selection, selectionArgs);

            return 0;
        }
        return db.insert(
                CreepypastaContract.CreepypastaEntry.NOME_TABELA,
                null,
                values);
    }

    public boolean verificarCreepyEstaFavoritada(String link){

        SQLiteDatabase db = creepypastaDbHelper.getReadableDatabase();

        String[] projection = { CreepypastaContract.CreepypastaEntry.COLUNA_LINK };
        String selection = CreepypastaContract.CreepypastaEntry.COLUNA_LINK + " = ?";
        String[] selectionArgs = { link };

        Cursor c = db.query(
                CreepypastaContract.CreepypastaEntry.NOME_TABELA,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                null);

        c.moveToFirst();

        if(c.getCount() > 0) {
            return  true;
        }
        return false;
    }

    public List<Creepypasta> buscarCreepysFavoritas(){
        List<Creepypasta> creepypastas = new ArrayList<>();

        SQLiteDatabase db = creepypastaDbHelper.getReadableDatabase();

        String[] projection = {
                CreepypastaContract.CreepypastaEntry._ID,
                CreepypastaContract.CreepypastaEntry.COLUNA_TITULO,
                CreepypastaContract.CreepypastaEntry.COLUNA_LINK
        };

        Cursor c = db.query(
                CreepypastaContract.CreepypastaEntry.NOME_TABELA,
                projection,
                null,
                null,
                null,
                null,
                null);

        while (c.moveToNext()){
//            long itemId = c.getLong(c.getColumnIndexOrThrow(CreepypastaContract.CreepypastaEntry._ID));
            Creepypasta creepypasta = new Creepypasta();
            creepypasta.setTitulo(
                    c.getString(c.getColumnIndexOrThrow(CreepypastaContract.CreepypastaEntry.COLUNA_TITULO)));
            creepypasta.setLink(
                    c.getString(c.getColumnIndexOrThrow(CreepypastaContract.CreepypastaEntry.COLUNA_LINK)));

            creepypastas.add(creepypasta);
        }

        return creepypastas;
    }
}
