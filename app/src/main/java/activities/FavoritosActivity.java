package activities;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

import adapters.favoritos.FavoritosAdapter;
import app.creepypastabrasil.com.br.creepypastabrasil.R;
import models.Creepypasta;
import repository.CreepypastaDAO;

public class FavoritosActivity extends AppCompatActivity {

    private RecyclerView recyclerViewFavoritos;
    private FavoritosAdapter favoritosAdapter;
    private ConstraintLayout constraintSemFavoritos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favoritos);
        setTitle("Favoritos");

        recyclerViewFavoritos = findViewById(R.id.recyclerFavoritos);
        constraintSemFavoritos = findViewById(R.id.constraintSemFavoritos);

        CreepypastaDAO creepypastaDAO = new CreepypastaDAO(getBaseContext());

        List<Creepypasta> creepypastas = creepypastaDAO.buscarCreepysFavoritas();

        if(creepypastas.size() > 0){
            constraintSemFavoritos.setVisibility(View.GONE);
        }

        favoritosAdapter = new FavoritosAdapter(creepypastas, this);
        recyclerViewFavoritos.setAdapter(favoritosAdapter);

        RecyclerView.LayoutManager layout = new GridLayoutManager(this,2);
        recyclerViewFavoritos.setLayoutManager(layout);
    }
}
