package models;

/**
 * Created by alan on 14/01/18.
 */

public class Creepypasta {

    private String data;
    private String titulo;
    private String intro;
    private String link;
    private boolean visitada;

    public Creepypasta() {
    }

    public Creepypasta(String data, String titulo, String intro) {
        this.data = data;
        this.titulo = titulo;
        this.intro = intro;
    }

    public String getData() {
        return data;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getIntro() {
        return intro;
    }

    public String getLink() {
        return link;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public boolean isVisitada() {
        return visitada;
    }

    public void setVisitada(boolean visitada) {
        this.visitada = visitada;
    }

    @Override
    public String toString() {
        return "Creepypasta{" +
                "data='" + data + '\'' +
                ", titulo='" + titulo + '\'' +
                ", intro='" + intro + '\'' +
                ", link='" + link + '\'' +
                ", visitada=" + visitada +
                '}';
    }
}
